//Define the DashboardApp module

var app = angular.module('DashboardApp', ['ui.router', 'angular-websocket', 'chart.js', 'widgetGrid', 'angularjs-dropdown-multiselect']);

app.config(function($stateProvider, $urlRouterProvider){

    //Provide a default route, just in case for unmatched urls
    $urlRouterProvider.otherwise("/DashboardSelection");

    //Set up routes so that you can traverse between pages easily
    $stateProvider
        .state('DashboardSelection',{
            url: "/DashboardSelection",
            templateUrl: "partials/DashboardSelection.html"
        })
        .state('Dashboard',{
            url: "/Dashboard",
            templateUrl: "partials/Dashboard.html"
        })
        .state('ConfigEditor',{
            url: "/ConfigEditor",
            templateUrl: "partials/ConfigEditor.html"
        })
        .state('WirelessTelemetry',{
            url: "/WirelessTelemetry",
            templateUrl: "partials/WirelessTelemetry.html"
        });    
});