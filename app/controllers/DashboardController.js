var app = angular.module('DashboardApp');

app.controller('DashboardCtrl', function($scope, $state, DashboardService) { 
   DashboardService.dashboardList().then(function(response) {
      var rawHTML = response.data;
      var availableDashboards = rawHTML.match(/href="(.*).dash"/g);
      for (i = 0; i < availableDashboards.length; i++) {
         // Getting rid of the /href=" and the .dash/
         // decodeURIComponent needed because spaces represented by %20 etc. in URLs
         var temp = decodeURIComponent(availableDashboards[i]);
         availableDashboards[i] = temp.substring(6, temp.length-6);
      }
      $scope.array = availableDashboards;
   });
   
   $scope.current = DashboardService.getCurrent();
   
   $scope.storeAndSwitch = function(newDashboard) {
      DashboardService.setCurrent(newDashboard);
      $scope.current = DashboardService.getCurrent();
      $state.go("Dashboard");
   };
});