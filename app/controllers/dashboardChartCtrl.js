var app = angular.module('DashboardApp');

app.controller('dashboardChartCtrl', function($scope, $interval, DataService) {
    $scope.isNotLocked = false
    $scope.text = "Unlock Dashboard";
    $scope.showGrid = false;

    $scope.data = new Array();
    $scope.labels = new Array();
    $scope.type = new Array();
    $scope.channelNum = new Array();
    $scope.channelName = new Array();

    $scope.selectedChannels = new Array();

    var data = new Array();
    var emptyCount = 0;

    var promise = $interval(reflow, 500);
    
    $scope.uploadEnabled = false;
    $scope.upload_btn = false;
    $scope.uploadText = "Upload";

    $scope.import = function() {
        var data = $scope.fileContent;

        $scope.uploadText = "Starting upload";

        data = data.replace(/\n/g, ",");

        data = "[" + data;

        //Replace last character with ] as it is currently a comma
        data = data.slice(0, -1) + "]";

        //Clear out previous data set
        DataService.clearData();

        data = JSON.parse(data);

        //Should perform some error checking
        if(!DataService.importData(data)){
          $scope.uploadText = "Upload Failed";          
        } else {
          $scope.uploadText = "Upload Finished";
          $scope.upload_btn = !$scope.upload_btn;
        }

        $scope.uploadEnabled = false;

        // Populates the channel selection dropdown
        populate();
    };

    $scope.$on('File Read Start', function() {
        $scope.uploadEnabled = false;
        $scope.uploadText = "Checking file";
    });

    $scope.$on('File Read End', function() {
        $scope.uploadEnabled = true;
        $scope.upload_btn = !$scope.upload_btn;
        console.log(this.btnState2);
        $scope.uploadText = "File ready";
    });


    function populate() {
        $scope.channels = DataService.getChannels();
        $scope.channelSettings = {
            scrollableHeight: '200px',
            scrollable: true
        };
    }

    $scope.toggle = function() {
        if ($scope.isNotLocked === true) {
            $scope.isNotLocked = false;
            $scope.text = "Unlock Dashboard";
            $scope.showGrid = false;
        } else {
            $scope.isNotLocked = true;
            $scope.text = "Lock Dashboard";
            $scope.showGrid = true;
        }
    };;

    // Example chart data and adding/removing functions
    $scope.charts = [];
    $scope.chartTypes = ['area', 'bubble', 'bar', 'column', 'gauge', 'line', 'pie', 'scatter', 'synchronised'];


    $scope.addChart = function(type) {

        $scope.charts.push({
            name: "Empty",
            display: true,
            id: emptyCount,
            position: {
                top: 1,
                height: 10,
                left: 1,
                width: 10
            },
            type: type
        });
        $scope.selectedChannels[emptyCount] = [];
        emptyCount++;

    };

    $scope.addChannel = function(chart) {
        var i = 0;
        var channelNum = new Array();
        var channelData = new Array();

        angular.forEach($scope.selectedChannels[chart.id], function(value, key) {
            channelNum[i] = $scope.selectedChannels[chart.id][i].id;
            channelData[i] = DataService.getHighChartsChannelData(channelNum[i]);
            i++;
        });

        if (chart.type == 'synchronised') {
            drawSynchronisedChart(chart, channelData, i);
        } else {
            drawChart(chart, channelData, i);
        }

    };

    function drawChart(chart, channelData, n) {

        $('#' + chart.id).empty();
        
        $('#' + chart.id).highcharts({
            chart: {
                type: chart.type,
                zoomType: 'xy',
                panning: true,
                panKey: 'shift',
                borderColor: '#346691',
                borderWidth: 2
            },
            subtitle: {
                text: 'Click and drag to zoom in. Hold down shift key to pan.'
            },
            xAxis: {
                title: {
                  enabled: true,
                  text: 'Time (s)'
                },
                categories: channelData[0].time
            },
            navigation: {
                buttonOptions: {
                    align: 'left'
                }
            }
        });

        var hChart = $('#' + chart.id).highcharts();
        var chartTitle = "";
        for (var i = 0; i < n; i++) {
            hChart.addSeries({
                name: channelData[i].channelName,
                data: channelData[i].data
            });
            chartTitle = chartTitle.concat(channelData[i].channelName);
            if (i != n - 1) {
                chartTitle = chartTitle.concat(" & ");
            }
        }

        if (channelData[0].diagLow < 1000) {
            hChart.yAxis[0].addPlotLine({
                label: {
                    text: 'Diag Low'
                },
                value: channelData[0].diagLow, 
                color: 'orange', 
                width: 2,
                zIndex: 5
            });
        }
        if (channelData[0].warnLow < 1000) {
            hChart.yAxis[0].addPlotLine({
                label: {
                    text: 'Warn Low'
                },
                value: channelData[0].warnLow, 
                color: 'red', 
                width: 2,                    
                zIndex: 10
            });
        }
        if (channelData[0].diagHigh > 0) {
            hChart.yAxis[0].addPlotLine({
                label: {
                    text: 'Diag High'
                },
                value: channelData[0].diagHigh, 
                color: 'orange', 
                width: 2,                    
                zIndex: 5
            });
        }
        if (channelData[0].warnHigh > 0) {
            hChart.yAxis[0].addPlotLine({
                label: {
                    text: 'Warn High'
                },
                value: channelData[0].warnHigh, 
                color: 'red', 
                width: 2,                    
                zIndex: 10
            });
        }

        hChart.title.update({
            text: chartTitle
        });
    };

    function drawSynchronisedChart(chart, channelData, n) {

        var chHeight = $('#' + chart.id).height() / n;
        $('#' + chart.id).empty();

        $(".chart").height(chHeight);

        $('#' + chart.id).bind('mousemove touchmove touchstart', function(e) {
            var chart,
                point,
                i,
                event;

            for (i = 0; i < Highcharts.charts.length; i = i + 1) {
                chart = Highcharts.charts[i];
                event = chart.pointer.normalize(e.originalEvent); // Find coordinates within the chart
                if (chart.series.length > 0) {
                    point = chart.series[0].searchPoint(event, true); // Get the hovered point

                    if (point) {
                        point.highlight(e);
                    }
                }
            }
        });
        /**
         * Override the reset function, we don't need to hide the tooltips and crosshairs.
         */
        Highcharts.Pointer.prototype.reset = function() {
            return undefined;
        };

        /**
         * Highlight a point by showing tooltip, setting hover state and draw crosshair
         */
        Highcharts.Point.prototype.highlight = function(event) {
            this.onMouseOver(); // Show the hover marker
            this.series.chart.tooltip.refresh(this); // Show the tooltip
            this.series.chart.xAxis[0].drawCrosshair(event, this); // Show the crosshair
        };

        /**
         * Synchronize zooming through the setExtremes event handler.
         */
        function syncExtremes(e) {
            var thisChart = this.chart;

            if (e.trigger !== 'syncExtremes') { // Prevent feedback loop
                Highcharts.each(Highcharts.charts, function(chart) {
                    if (chart !== thisChart) {
                        if (chart.xAxis[0].setExtremes) { // It is null while updating
                            chart.xAxis[0].setExtremes(e.min, e.max, undefined, false, {
                                trigger: 'syncExtremes'
                            });
                        }
                    }
                });
            }
        }

        for (var i = 0; i < n; i++) {
            $('<div id='+chart.type+''+channelData[i].channelNum+'>').appendTo('#' + chart.id).highcharts({
                chart: {
                    marginLeft: 40, // Keep all charts left aligned
                    spacingBottom: 55,
                    height: chHeight, 
                    zoomType: 'xy',
                    panning: true,
                    panKey: 'shift',
                    borderColor: '#346691',
                    borderWidth: 2
                },
                title: {
                    text: channelData[i].channelName,
                    align: 'center',
                    margin: 0,
                    x: 30
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                xAxis: {
                    crosshair: {
                        width: 3,
                        color: 'black'
                    },
                    events: {
                        setExtremes: syncExtremes
                    },
                    categories: channelData[i].time, 
                },
                yAxis: {
                    title: {
                        text: null
                    },
                },
                exporting: {
                    enabled: false
                },
                tooltip: {
                    positioner: function() {
                        return {
                            x: this.chart.chartWidth - this.label.width, // right aligned
                            y: -1 // align to title
                        };
                    },
                    borderWidth: 0,
                    backgroundColor: 'none',
                    pointFormat: '{point.y}',
                    headerFormat: '',
                    shadow: false,
                    style: {
                        fontSize: '18px'
                    }
                },
                series: [{
                    data: channelData[i].data,
                    name: channelData[i].channelName,
                    type: 'line',
                    color: Highcharts.getOptions().colors[i],
                    fillOpacity: 0.3,
                }]
            });
        }
    }

    $scope.removeChart = function(chart) {
        var index = $scope.charts.indexOf(chart);
        $scope.charts.splice(index, 1);
    };

    function reflow() {
        angular.forEach($scope.charts, function(value, key) {
            var chart = $('#' + value.id).highcharts();
            if (chart != null) {
                chart.reflow();
            }
            else {
                if (value.type == 'synchronised') {
                    var childrenChart = new Array();
                     $('div','#' + value.id).each(function(){
                        childrenChart.push($(this).attr('id')); 
                    });

                    var chHeight = $('#' + value.id).height() / (childrenChart.length/2);
                    var width = $('#' + value.id).width();
                    angular.forEach(childrenChart, function(value, key) {
                        if (value.indexOf("highcharts") == -1) {
                            var chart = $('#' + value).highcharts();
                            if (chart != null) {
                                chart.setSize(width, Math.floor(chHeight), false);
                            }
                        }
                    })
                }
                
            }
        })
    }

    function mockData(channelNum, channelName) {

        var channelData = {};
        channelData.channelNum = channelNum;

        channelData.channelName = channelName;

        channelData.data = [5 * Math.random(), 5 * Math.random(), 5 * Math.random(), 5 * Math.random(), 5 * Math.random(), 5 * Math.random(), 5 * Math.random(), 5 * Math.random(), 5 * Math.random(), 5 * Math.random()];
        channelData.time = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0];

        return channelData;
    };

});