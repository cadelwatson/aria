var app = angular.module('DashboardApp');

app.controller('ImportController', function($scope, DataService){
	$scope.uploadEnabled = false;
	$scope.uploadText = "";

	$scope.import = function(){
		var data = $scope.fileContent;

		$scope.uploadText = "Starting upload";

		data = data.replace(/\n/g, ",");

		data = "[" + data;

		//Replace last character with ] as it is currently a comma
		data = data.slice(0,-1) + "]";

		//Clear out previous data set
		DataService.clearData();

		//Should perform some error checking
		DataService.importData(data);

		$scope.uploadText = "Upload Finished";
		$scope.uploadEnabled = false;
	};

	$scope.$on('File Read Start', function(){
		$scope.uploadEnabled = false;
		$scope.uploadText = "Checking file";
	});

	$scope.$on('File Read End', function(){
		$scope.uploadEnabled = true;
		$scope.uploadText = "File ready";
	});

});