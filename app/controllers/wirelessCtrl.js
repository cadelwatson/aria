var app = angular.module('DashboardApp');

app.controller('WirelessController', function($scope, $websocket, $window, $interval){
	var dataStream = null;
	var promise;
	var ip_add;
	var port_num;
   $scope.initialOptions = {
      chart: {
         type: 'line',
      },
      legend: {
         enabled: false
      },
      title: {
         text: null
      },
      xAxis: {
         categories: []
      },
      yAxis: {
      },
      series: [{
         data: []
      }]
   };
   $scope.channels = [];
   $scope.chartTypes = ['areaspline', 'column', 'line', 'pie', 'scatter'];

   // Controls button which toggles between real and mock data
   $scope.dataSource = "Using telemetry";
   var useTelemetry = true;
   $scope.toggleSource = function() {
      if (useTelemetry == true) {
         useTelemetry = false;
         $scope.dataSource = "Using mock data";
      } else {
         useTelemetry = true;
         $scope.dataSource = "Using telemetry";
      }
   }
   
   // Controls button which starts and stops streaming of data
	$scope.btnText = "Start Streaming";
	$scope.toggle = function () {
	   this.btnState = !this.btnState;
		if (this.btnState == true) {
			this.startStreaming();
			$scope.btnText = "Stop Streaming";
		}
		else {
			this.destroyConnection();
			$scope.btnText = "Start Streaming";
		}
	};
   
	$scope.startStreaming = function() {
      var data;
      // The created promise runs this function every second to request for data
		promise = $interval(function() {
         if (useTelemetry == true) {
            if (dataStream == null) {
               $scope.createConnection();
            }
            if (dataStream !== null) {
               dataStream.send(JSON.stringify({request: '1'})); // Send request to server
            }
            dataStream.onMessage(function(message) {
               data = JSON.parse(message.data);
               data = data.response.data;
            });
         } else {
            data = mockData();
         }
         for (var i = 0; i < data.length; i++) {
            var j = 0;
            while (j < $scope.channels.length) {
               if ($scope.channels[j].id == "Channel"+data[i].channel) {
                  break;
               }
               j++;
            }
            if (j == $scope.channels.length) {
               $scope.channels.push(new Channel("Channel"+data[i].channel, data[i].channelName));
            }
         };
         drawData(data);
      }, 1000);
	}

   function Point(color, y) {
      this.marker = {fillColor: color};
      this.color = color;
      this.y = y;
   }
   
   function Channel(id, name) {
      this.id = id;
      this.name = name;
      this.points = [];
   }
   
   var drawData = function(data) {
      for (var i = 0; i < $scope.channels.length; i++) {
         var graph = $('#' + $scope.channels[i].id).highcharts();
         var highlight;
         if (data[i].diag == true) {
            highlight = "#f45b5b";
         } else if (data[i].warn == true) {
            highlight = "#e4d354";
         } else {
            highlight = "#7cb5ec";
         }
         var shiftPointsEnabled;
         if (graph.series[0].data.length > 30) {
            shiftPointsEnabled = true;
         } else {
            shiftPointsEnabled = false;
         }
         graph.series[0].addPoint( {marker:{fillColor: highlight}, y: data[i].value, color: highlight}, true, shiftPointsEnabled, false );
         $scope.channels[i].points.push(new Point(highlight, data[i].value));
         if (shiftPointsEnabled == true) {
            $scope.channels[i].points.shift();
         }
         console.log($scope.channels[0].points);
      };
   }
   
	$scope.createConnection = function() {
		var ws = "ws://";
		if (ip_add == null) {
			ip_add = $window.wirelessDetails.ip_addr;
		}
		
		if (port_num == null) {
			port_num = $window.wirelessDetails.port;
		}

		console.log(ip_add);
		console.log(port_num);
		url = ws.concat(ip_add,":",port_num);
		dataStream = $websocket(url);
		dataStream.initialTimeout = 10;
	};

	$scope.destroyConnection = function() {
		$interval.cancel(promise);
		if (dataStream != null) {
			dataStream.close();
			dataStream = null;
		}
	};

	$scope.connectionDetails = function() {
		this.connectionState = !this.connectionState;
	};

	$scope.saveConnectionDetails = function() {
		this.toggle();
		ip_add = this.IP_add;
		port_num = this.port_num;
		this.connectionDetails();
	};
   
   $scope.changeChartType = function(channelID, newType) {
      var graph = $('#' + channelID).highcharts();
      graph.series[0].update({
            type: newType,
      }, false);
      
      var i = 0;
      while (i < $scope.channels.length) {
         if ($scope.channels[i].id == channelID) {
            break;
         }
         i++;
      }
      graph.series[0].setData(angular.copy($scope.channels[i].points));
      // Angular.copy is used since the data array is passed by reference to setData
      // Unusual or excessive data was being added when graph data was changed by Highcharts functions
      // Passing a new copy each time mitigates this issue
      console.log($scope.channels[i].points);
   };
   
	/* Used for testing when not connected to parallella */
	function mockData() {
		var mData = new Array();
		mData[0] = {};
		mData[0].channel = 1;
		mData[0].time = 0;
		mData[0].value = 5*Math.random();
		mData[0].channelName = "Temp 1";
      mData[0].diag =  Math.random() >= 0.5;
      mData[0].warn =  Math.random() >= 0.5;

		mData[1] = {};
		mData[1].channel = 2;
		mData[1].time = 0;
		mData[1].value = 5*Math.random();
		mData[1].channelName = "Pressure 6";
      mData[1].diag =  Math.random() >= 0.5;
      mData[1].warn =  Math.random() >= 0.5;

		mData[2] = {};
		mData[2].channel = 3;
		mData[2].time = 0;
		mData[2].value = 5*Math.random();
		mData[2].channelName = "Area 51";
      mData[2].diag =  Math.random() >= 0.5;
      mData[2].warn =  Math.random() >= 0.5;		

		return mData;
	}
});