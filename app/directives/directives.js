var app = angular.module('DashboardApp');

app.directive("highchartsGraph", function() {
   return {
      scope: {
         options: '=',
      },
      link: function(scope, element, attrs) {
         Highcharts.chart(element[0], scope.options);
      }
   };
});

app.directive('dashboardDir', function() {
	return {
		templateUrl: 'directives/dashboardChart.html'
	};

});

app.directive('drawDir', function() {
  return function(scope, element, attrs) {
    console.log("Draw")
  };
})

app.directive('fileReader', function() {
  return {
    scope: {
      fileReader:"="
    },
    link: function(scope, element) {
      $(element).on('change', function(changeEvent) {
        var files = changeEvent.target.files;
        if (files.length) {
          var r = new FileReader();
          r.onload = function(e) {
              var contents = e.target.result;
              scope.$apply(function () {
                scope.fileReader = contents;
              });
          };
          
          scope.$emit('File Read Start');

          //Reads a single file
          r.readAsText(files[0]);

          scope.$emit('File Read End');
        }
      });
    }
  };
});