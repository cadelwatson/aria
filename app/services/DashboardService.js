var app = angular.module('DashboardApp');

app.service('DashboardService', function($http) {
   this.dashboardList = function() {
      var folderAddress = "http://" + location.host + "/dashboards/";
      return $http.get(folderAddress)
   }
   
    var current = null;
    this.getCurrent = function() {
        return current;
    }
    
    this.setCurrent = function(newDashboard) {
       current = newDashboard;
    }
});