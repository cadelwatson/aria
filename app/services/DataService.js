var app = angular.module('DashboardApp');


/* Currently designed to work for a single data set
 * Needs to be scaled to work for multiple data sets
 */
app.service('DataService', function() {

   /* The data is stored in individual objects, the same as the data file
    * {channelName, channelnum, value, time, diag, warn}
    */

   var fdb = new ForerunnerDB();

   /* Allows you to import data into database
    * Uses format: [{channelName,channelnum,diag,time,value,warn}]
    * Returns true or false based on success of operation
    */
   this.importData = function(data){
      var result = false;

      if(data instanceof Array){
         fdb.db('ShadowBroker').collection('data').deferredCalls(false);
         fdb.db('ShadowBroker').collection('data').insert(data);
         result = true;
      }

      return result;
   };

   this.clearData = function(){
      fdb.db('ShadowBroker').collection('data').drop();
   };

   this.getAllData = function(){
      return fdb.db('ShadowBroker').collection('data').find();
   };

   /* Returns a specified channel data for a given channel number
    * If channel number isn't found in database, empty dataset returned
    */
   this.getChannelData = function(channel){
      var result = [];
      
      result = fdb.db('ShadowBroker').collection('data').find({
            channelnum:{
               $eq:channel
            }
         },
         {
            $aggregate : "value"
         }
      );
      
      return result;
   }

   /* Returns the associated channel time for a given channel number
    * If channel number isn't found in database, empty dataset returned
    */
   this.getChannelTime = function(channel){
      var result = [];

      result = fdb.db('ShadowBroker').collection('data').find({
            channelnum:{
               $eq:channel
            }
         },
         {
            $aggregate : "time"
         }
      );

      return result;
   }

   /* Returns the channel name for a given channel number
    * If channel number isn't found, empty string returned
    * TODO: Improve query
    */
   this.getChannelName = function(channel){
      var result = "";
      var tmp;

      tmp = fdb.db('ShadowBroker').collection('data').find({
            channelnum:{
               $eq:channel
            }
         },
         {
            $aggregate : "channelname"
         }
      );

      //Just take the first result
      result = tmp[0];

      return result;
   }

   /* Returns the channel diagnostic for a given channel number
    * If channel number isn't found, empty string returned
    */
   this.getChannelDiag = function(channel){
      var result = [];

      result = fdb.db('ShadowBroker').collection('data').find({
            channelnum:{
               $eq:channel
            }
         },
         {
            $aggregate : "diag"
         }
      );

      return result;
   }

   /* Returns the channel warning bools for a given channel number
    * If channel number isn't found, empty string returned
    */
   this.getChannelWarn = function(channel){
      var result = [];

      result = fdb.db('ShadowBroker').collection('data').find({
            channelnum:{
               $eq:channel
            }
         },
         {
            $aggregate : "warn"
         }
      );

      return result;
   }

   /* Returns a list of all channel names inside the collection
    * If no channel exists, empty set returned
    * Returns it in a numerical ID list, as required by dashboardController
    * [{"id": sequential id, "label":channelname}]
    */
   this.getChannels = function(){
      var result = [];
      var tmp1;
      var tmp2;

      tmp1 = fdb.db('ShadowBroker').collection('data').find({},{
         $aggregate: "channelname"
      });

      tmp1 = tmp1.filter(function(elem, index, self) {
         return index == self.indexOf(elem);
      });

      tmp2 = fdb.db('ShadowBroker').collection('data').find({},{
         $aggregate: "channelnum"
      });


      for(var i = 0; i < tmp1.length; i++){
         result.push({
            id: tmp2[i],
            label: tmp1[i]
         });
      }

      return result;
   }

   /* Returns the channel number corresponding to a particular channel name
    * Returns -1 if it cannot find the channel name
    * TODO: Improve query
    */
   this.convertNameToNumber = function(name){
      var result = -1;
      var tmp;

      tmp = fdb.db('ShadowBroker').collection('data').find({
         channelname:{
            $eq:name
         }
      },
      {
         $aggregate: "channelnum"
      });

      result = tmp[0];

      return result;
   }

   /* Specific function designed to output a channel data object
    * As preferred by HighCharts
    */
   this.getHighChartsChannelData = function(channelNum){
      var result = {};

      result.channelNum = channelNum;

      result.channelName = this.getChannelName(channelNum);

      result.data = this.getChannelData(channelNum);
      result.time = this.getChannelTime(channelNum);

      var diag = this.getChannelDiag(channelNum);
      var warn = this.getChannelWarn(channelNum);
      result.diagLow = 1000;
      result.diagHigh = 0;
      result.warnLow = 1000;
      result.warnHigh = 0;

      result.color = new Array();

      for(var i = 0; i < result.time.length; i++) {
        result.time[i] = result.time[i].toFixed(0);
        if (diag[i] == true && result.data[i] < result.diagLow) {
          // This is a diagnostic value
          result.diagLow = result.data[i];
        }
        else if (diag[i] == true && result.data[i] > result.diagHigh) {
          // This is a diagnostic value
          result.diagHigh = result.data[i];
        }
        else if (warn[i] == true && result.data[i] < result.warnLow) {
          // This is a warning value
          result.warnLow = result.data[i];
        }
        else if (warn[i] == true && result.data[i] > result.warnHigh) {
          // This is a warning value
          result.warnHigh = result.data[i];
        }

      }   

      if (Math.abs(result.diagLow - result.warnLow) < 0.01) {
        result.diagLow = 1000;
      }
      if (Math.abs(result.diagHigh - result.warnHigh) < 0.01) {
        result.diagHigh = 0;
      }   
      return result;
   }

});

